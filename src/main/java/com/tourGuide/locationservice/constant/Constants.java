package com.tourGuide.locationservice.constant;

/** Contains constants. */
public class Constants {

  public static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;
  public static final double LONGITUDE_NEGATIVE_LIMIT = -180.0;
  public static final double LONGITUDE_POSITIVE_LIMIT = 180.0;
  public static final double LATITUDE_NEGATIVE_LIMIT = -90.0;
  public static final double LATITUDE_POSITIVE_LIMIT = 90.0;
}
