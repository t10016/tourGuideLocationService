package com.tourGuide.locationservice.config;

import gpsUtil.GpsUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Configuration for bean. */
@Configuration
public class BeanConfiguration {
  @Bean
  public GpsUtil gpsUtil() {
    return new GpsUtil();
  }
}
