package com.tourGuide.locationservice.repository;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.springframework.stereotype.Repository;

/** Repository for Location. Contains method to save/read VisitedLocation. */
@Repository
public class LocationRepository {
  private final Map<UUID, CopyOnWriteArrayList<VisitedLocation>> internalLocationMap =
      new ConcurrentHashMap<>();
  private final GpsUtil gpsUtil;

  public LocationRepository(GpsUtil gpsUtil) {
    this.gpsUtil = gpsUtil;
  }

  /**
   * Save the visitedLocation. save visitedLocation in a Map with key=userId,
   * value=List<visitedLocation> if there is no entry for the given id : create new
   *
   * @param visitedLocation the visitedLocation to add
   * @return the visitedLocation
   */
  public VisitedLocation addVisitedLocation(VisitedLocation visitedLocation) {
    List<VisitedLocation> visitedLocations = internalLocationMap.get(visitedLocation.userId);
    if (visitedLocations == null) {
      CopyOnWriteArrayList<VisitedLocation> newList = new CopyOnWriteArrayList<>();
      newList.add(visitedLocation);
      internalLocationMap.put(visitedLocation.userId, newList);
    } else {
      visitedLocations.add(visitedLocation);
    }
    return visitedLocation;
  }

  /**
   * Get all visitedLocation saved for the given user id.
   *
   * @param userId the user id
   * @return collection of VisitedLocation, return an empty list if there is none
   */
  public Collection<VisitedLocation> getAllVisitedLocations(UUID userId) {

    if (!internalLocationMap.containsKey(userId)) {
      return new ArrayList<>();
    }
    return internalLocationMap.get(userId);
  }

  /**
   * Get all VisitedLocation stored. return a MultiMap with userId : Collection of VisitedLocation
   */
  public Multimap<UUID, VisitedLocation> getAllLocations() {

    Multimap<UUID, VisitedLocation> multimap = ArrayListMultimap.create();
    for (Map.Entry<UUID, CopyOnWriteArrayList<VisitedLocation>> entry :
        internalLocationMap.entrySet()) {
      for (VisitedLocation v : entry.getValue()) {
        multimap.put(v.userId, v);
      }
    }
    return multimap;
  }

  public List<Attraction> getAllAttractions() {

    return gpsUtil.getAttractions();
  }
}
