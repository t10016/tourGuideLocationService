package com.tourGuide.locationservice.controller;

import com.tourGuide.locationservice.constant.Constants;
import com.tourGuide.locationservice.exception.IllegalArgumentException;
import com.tourGuide.locationservice.model.AttractionWithDistanceDto;
import com.tourGuide.locationservice.service.LocationServices;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/** Rest Controller for Location Api. */
@RestController
public class LocationController {

  private final Logger logger = LoggerFactory.getLogger(LocationController.class);
  @Autowired private LocationServices locationService;

  /**
   * Add the current location of the given user as Visited location.
   *
   * @param userId the user id.
   * @return the new location added.
   */
  @PostMapping("/location/add")
  @ResponseStatus(HttpStatus.CREATED)
  public VisitedLocation addLocation(@RequestParam UUID userId)
      throws ExecutionException, InterruptedException {

    return locationService.addLocation(userId).join();
  }

  /**
   * Get all saved VisitedLocation for the given user.
   *
   * @param userId the user id
   * @return collection of visitedLocation, can throw ResourceNotFoundException
   */
  @GetMapping("/location/getAll")
  public Collection<VisitedLocation> getAllVisitedLocation(@RequestParam UUID userId)
      throws InterruptedException {
    return locationService.getAllVisitedLocation(userId);
  }

  /**
   * Get the last Location saved for each user.
   *
   * @return a map of userId : Location, can throw ResourceNotFoundException
   */
  @GetMapping("/getStats")
  public Map<UUID, Location> getAllLastLocation() {
    Collection<VisitedLocation> allUserLatestVisitedLocation =
        locationService.getAllUserLatestVisitedLocation();
    Map<UUID, Location> map = new LinkedHashMap<>();
    for (VisitedLocation temp : allUserLatestVisitedLocation) {
      map.put(temp.userId, temp.location);
    }

    return map;
  }

  /**
   * Get the 5 closest Attraction of the location(latitude,longitude).
   *
   * @param latitude the latitude (-90;90)
   * @param longitude the longitude (-180;180)
   * @return Collection of attraction ordered by distance, can throw
   *     IllegalArgumentException/ResourceNotFoundException
   */
  @GetMapping("/attraction/getNearby")
  public Collection<AttractionWithDistanceDto> getNearbyAttractions(
      @RequestParam Double latitude, @RequestParam Double longitude) {

    if (latitude > Constants.LATITUDE_POSITIVE_LIMIT
        || latitude < Constants.LATITUDE_NEGATIVE_LIMIT
        || longitude < Constants.LONGITUDE_NEGATIVE_LIMIT
        || longitude > Constants.LONGITUDE_POSITIVE_LIMIT) {
      logger.warn(
          "error, illegal argument for latitude : " + latitude + " ; longitude : " + longitude);
      throw new IllegalArgumentException("error, argument out of earth");
    }

    return locationService.getNearbyAttractions(latitude, longitude);
  }

  /**
   * Get all attractions.
   *
   * @return collection of Attraction, can throw ResourceNotFoundException
   */
  @GetMapping("/attraction/getAll")
  public Collection<Attraction> getAllAttractions() {
    return locationService.getAllAttractions();
  }

  /**
   * Get the closest Attraction for the given location (latitude, longitude).
   *
   * @param latitude the latitude (-90;90)
   * @param longitude the longitude (-180;180)
   * @return dto object of Attraction with an additional distance field, can throw
   *     IllegalArgumentException, ResourceNotFoundException
   */
  @GetMapping("/attraction/getClosest")
  public AttractionWithDistanceDto getClosestAttraction(Double latitude, Double longitude) {

    if (latitude == null || longitude == null || latitude.isNaN() || longitude.isNaN()) {
      logger.warn(
          "Error, latitude and longitude are mandatory. latitude : "
              + latitude
              + " longitude : "
              + longitude);
      throw new IllegalArgumentException("Error, latitude and longitude are mandatory.");
    }

    return locationService.getClosestAttraction(latitude, longitude);
  }
}
