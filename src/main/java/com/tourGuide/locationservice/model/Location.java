package com.tourGuide.locationservice.model;

/**
 * Record for Location object.
 *
 * @param latitude the latitude
 * @param longitude the longitude
 */
public record Location(double latitude, double longitude) {
}
