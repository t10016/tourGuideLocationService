package com.tourGuide.locationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/** PSV Main for LocationService Api. */
@SpringBootApplication
@EnableFeignClients
public class LocationserviceApplication {

  public static void main(String[] args) {
    java.util.Locale.setDefault(java.util.Locale.US);
    SpringApplication.run(LocationserviceApplication.class, args);
  }
}
