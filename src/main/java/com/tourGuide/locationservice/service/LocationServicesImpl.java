package com.tourGuide.locationservice.service;

import com.google.common.collect.Multimap;
import com.tourGuide.locationservice.constant.Constants;
import com.tourGuide.locationservice.exception.ResourceNotFoundException;
import com.tourGuide.locationservice.model.AttractionWithDistanceDto;
import com.tourGuide.locationservice.repository.LocationRepository;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Implementation for LocationService. Contains method used by LocationController */
@Service
public class LocationServicesImpl implements LocationServices {

  private final Logger logger = LoggerFactory.getLogger(LocationServicesImpl.class);
  private final ExecutorService executorService = Executors.newFixedThreadPool(150);
  @Autowired private GpsUtil gpsUtil;
  @Autowired private LocationRepository locationRepository;

  /**
   * This method add a new visited location for the user.
   *
   * @param userId the user ID
   * @return the object saved
   */
  @Override
  public CompletableFuture<VisitedLocation> addLocation(UUID userId) throws InterruptedException {

    CompletableFuture<VisitedLocation> visitedLocationCompletableFuture =
        CompletableFuture.supplyAsync(() -> gpsUtil.getUserLocation(userId), executorService)
            .thenApply((v) -> locationRepository.addVisitedLocation(v));

    while (!visitedLocationCompletableFuture.isDone()) {
      TimeUnit.MILLISECONDS.sleep(200);
    }

    return visitedLocationCompletableFuture;
  }

  /**
   * This method get all the location visited by the user. If there are none, save and return the
   * actual location.
   *
   * @param userId the user ID
   * @return a list which contains all the location visited by the user
   */
  @Override
  public Collection<VisitedLocation> getAllVisitedLocation(UUID userId)
      throws InterruptedException {

    Collection<VisitedLocation> visitedLocations =
        new ArrayList<>(locationRepository.getAllVisitedLocations(userId));

    // if there is no VisitedLocation -> add actual location as visited
    if (visitedLocations.isEmpty()) {
      CompletableFuture<VisitedLocation> presentLocation = this.addLocation(userId);
      try {
        visitedLocations.add(presentLocation.get());
      } catch (InterruptedException | ExecutionException e) {
        throw new RuntimeException(e);
      }
    }

    return visitedLocations;
  }

  /**
   * This method get the latest known location for every user.
   *
   * @return a list with all latest location.
   */
  @Override
  public Collection<VisitedLocation> getAllUserLatestVisitedLocation() {

    Multimap<UUID, VisitedLocation> allLocations = locationRepository.getAllLocations();

    if (allLocations.isEmpty()) {
      logger.warn("error, repository returned an empty MultiMap.");
      throw new ResourceNotFoundException("error, can't retrieve data.");
    }

    Collection<VisitedLocation> lastVisitedLocations = new ArrayList<>();

    for (UUID userId : allLocations.keySet()) {
      VisitedLocation lastVisitedLocation = null;
      for (VisitedLocation location : allLocations.get(userId)) {
        if (lastVisitedLocation == null
            || lastVisitedLocation.timeVisited.before(location.timeVisited)) {
          lastVisitedLocation = location;
        }
      }
      lastVisitedLocations.add(lastVisitedLocation);
    }
    logger.info("returning " + lastVisitedLocations.size() + " location history.");
    return lastVisitedLocations;
  }

  /**
   * Find 5 attractions closest to the given location.
   *
   * @param latitude the latitude Double(-90.0, 90.0)
   * @param longitude the longitude Double(-180.0, 180.0)
   * @return a list with all nearby attractions
   */
  @Override
  public Collection<AttractionWithDistanceDto> getNearbyAttractions(
      Double latitude, Double longitude) {

    TreeMap<Double, Attraction> map = getAllAttractionOrderedByDistance(latitude, longitude);
    Collection<AttractionWithDistanceDto> nearbyAttractions = new LinkedList<>();

    // create a list and populate with the first five objects of the TreeMap
    Iterator<Map.Entry<Double, Attraction>> attractionIterator = map.entrySet().iterator();
    for (int i = 0; attractionIterator.hasNext() && i < 5; i++) {
      Map.Entry<Double, Attraction> entry = attractionIterator.next();

      Attraction temp = entry.getValue();
      AttractionWithDistanceDto attractionDto = new AttractionWithDistanceDto();
      attractionDto.setAttractionId(temp.attractionId);
      attractionDto.setAttractionName(temp.attractionName);
      attractionDto.setCity(temp.city);
      attractionDto.setState(temp.state);
      attractionDto.setLocation(
          new com.tourGuide.locationservice.model.Location(temp.latitude, temp.longitude));
      attractionDto.setDistance(entry.getKey());
      nearbyAttractions.add(attractionDto);
    }

    return nearbyAttractions;
  }

  /**
   * This method get all attractions.
   *
   * @return a list with all attractions
   */
  @Override
  public Collection<Attraction> getAllAttractions() {

    List<Attraction> attractions = locationRepository.getAllAttractions();
    if (attractions.isEmpty()) {
      logger.warn("error, repository returned an empty List.");
      throw new ResourceNotFoundException("error, can't retrieve data.");
    }

    return attractions;
  }

  /**
   * This method find the closest attraction near the given location.
   *
   * @param latitude the latitude Double(-90.0, 90.0)
   * @param longitude the longitude Double(-180.0, 180.0)
   * @return the closest attraction and the distance to the location (double)
   */
  @Override
  public AttractionWithDistanceDto getClosestAttraction(double latitude, double longitude) {

    TreeMap<Double, Attraction> map = getAllAttractionOrderedByDistance(latitude, longitude);
    Attraction temp = map.firstEntry().getValue();
    AttractionWithDistanceDto closest =
        new AttractionWithDistanceDto(
            temp.attractionName,
            temp.city,
            temp.state,
            new com.tourGuide.locationservice.model.Location(temp.latitude, temp.longitude),
            map.firstKey());
    closest.setAttractionId(temp.attractionId);

    return closest;
  }

  /**
   * Get all attractions ordered by distance from given location (latitude, longitude).
   *
   * @param latitude the latitude
   * @param longitude the longitude
   * @return an ordered map of distance : attraction
   */
  private TreeMap<Double, Attraction> getAllAttractionOrderedByDistance(
      double latitude, double longitude) {
    List<Attraction> attractionList = locationRepository.getAllAttractions();

    if (attractionList.isEmpty()) {
      logger.warn("error, repository returned an empty List.");
      throw new ResourceNotFoundException("error, can't retrieve data.");
    }

    Location userLocation = new Location(latitude, longitude);

    TreeMap<Double, Attraction> map = new TreeMap<>();

    // get distance to user for each attraction
    // order map by distance (with TreeMap)
    for (Attraction attraction : attractionList) {
      double distance =
          Math.abs(
              getDistance(userLocation, new Location(attraction.latitude, attraction.longitude)));
      map.put(distance, attraction);
    }
    return map;
  }

  /**
   * Calculate the distance between 2 coordinate.
   *
   * @param loc1 location 1 : latitude, longitude
   * @param loc2 location 2 : latitude, longitude
   * @return distance en Miles
   */
  public double getDistance(Location loc1, Location loc2) {
    double lat1 = Math.toRadians(loc1.latitude);
    double lon1 = Math.toRadians(loc1.longitude);
    double lat2 = Math.toRadians(loc2.latitude);
    double lon2 = Math.toRadians(loc2.longitude);

    double angle =
        Math.acos(
            Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

    double nauticalMiles = 60 * Math.toDegrees(angle);
    return Constants.STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
  }
}
