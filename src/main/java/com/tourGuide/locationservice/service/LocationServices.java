package com.tourGuide.locationservice.service;

import com.tourGuide.locationservice.model.AttractionWithDistanceDto;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/** Interface for LocationService. Contains method used by LocationController. */
public interface LocationServices {

  /**
   * This method add a new visited location for the user.
   *
   * @param userId the user ID
   * @return the object saved
   */
  CompletableFuture<VisitedLocation> addLocation(UUID userId)
      throws ExecutionException, InterruptedException;

  /**
   * This method get all the location visited by the user.
   *
   * @param userId the user ID
   * @return a list which contains all the location visited by the user
   */
  Collection<VisitedLocation> getAllVisitedLocation(UUID userId) throws InterruptedException;

  /**
   * This method get the latest known location for every user.
   *
   * @return a list with all latest location.
   */
  Collection<VisitedLocation> getAllUserLatestVisitedLocation();

  /**
   * This method find the attractions near the given location.
   *
   * @param latitude the latitude Double(-90.0, 90.0)
   * @param longitude the longitude Double(-180.0, 180.0)
   * @return a list with all nearby attractions
   */
  Collection<AttractionWithDistanceDto> getNearbyAttractions(Double latitude, Double longitude);

  /**
   * This method get all attractions.
   *
   * @return a list with all attractions
   */
  Collection<Attraction> getAllAttractions();

  /**
   * This method find the closest attraction near the given location.
   *
   * @param latitude the latitude Double(-90.0, 90.0)
   * @param longitude the longitude Double(-180.0, 180.0)
   * @return the closest attraction and the distance to the location (double)
   */
  AttractionWithDistanceDto getClosestAttraction(double latitude, double longitude);
}
