package com.tourGuide.locationservice.service;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.tourGuide.locationservice.exception.ResourceNotFoundException;
import com.tourGuide.locationservice.model.AttractionWithDistanceDto;
import com.tourGuide.locationservice.repository.LocationRepository;
import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.assertj.core.data.Offset;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.withinPercentage;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LocationServicesImplTest {

  private static List<Attraction> attractions;

  @Mock private GpsUtil gpsUtilMock;
  @Mock private LocationRepository locationRepositoryMock;
  @InjectMocks LocationServicesImpl locationServices;

  @BeforeAll
  static void beforeAll() {
    attractions = new ArrayList<>();
    attractions.add(new Attraction("close1", "Anaheim", "CA", 0, 0));
    attractions.add(new Attraction("close2", "Jackson Hole", "WY", 1, -1));
    attractions.add(new Attraction("close4", "Kelso", "CA", 5, -5));
    attractions.add(new Attraction("close3", "Joshua Tree National Park", "CA", -1, -5));
    attractions.add(new Attraction("close5", "St Joe", "AR", 35.985512, -50));
    attractions.add(new Attraction("far1", "Hot Springs", "AR", 34.52153, -93.042267));
    attractions.add(new Attraction("far2", "Benson", "AZ", 31.837551, -110.347382));
  }

  @Test
  void addLocation() throws ExecutionException, InterruptedException {

    // GIVEN
    UUID userId = UUID.randomUUID();
    Date date = new Date();
    VisitedLocation visitedLocation = new VisitedLocation(userId, new Location(1.0, 1.0), date);
    // WHEN
    when(gpsUtilMock.getUserLocation(userId)).thenReturn(visitedLocation);
    when(locationRepositoryMock.addVisitedLocation(Mockito.any())).thenReturn(visitedLocation);
    locationServices.addLocation(userId).get();
    // THEN
    verify(gpsUtilMock, times(1)).getUserLocation(userId);
    verify(locationRepositoryMock, times(1)).addVisitedLocation(visitedLocation);
  }

  @Test
  void getAllVisitedLocation() throws InterruptedException {

    // GIVEN
    UUID userId = UUID.randomUUID();
    Date date = new Date();
    VisitedLocation visitedLocation1 = new VisitedLocation(userId, new Location(1.0, 1.0), date);
    VisitedLocation visitedLocation2 = new VisitedLocation(userId, new Location(2.0, 5.0), date);
    Collection<VisitedLocation> expected = Arrays.asList(visitedLocation1, visitedLocation2);
    // WHEN
    when(locationRepositoryMock.getAllVisitedLocations(userId))
        .thenReturn(Arrays.asList(visitedLocation1, visitedLocation2));
    Collection<VisitedLocation> actual = locationServices.getAllVisitedLocation(userId);
    // THEN
    verify(locationRepositoryMock, times(1)).getAllVisitedLocations(userId);
    assertTrue(actual.containsAll(expected));
  }

  @Test
  void getAllVisitedLocation_WhenNoData_ShouldReturnActualLocation() throws InterruptedException {

    // GIVEN
    UUID userId = UUID.randomUUID();
    Date date = new Date();
    VisitedLocation visitedLocation1 = new VisitedLocation(userId, new Location(1.0, 1.0), date);

    Collection<VisitedLocation> expected = List.of(visitedLocation1);

    // WHEN
    when(locationRepositoryMock.getAllVisitedLocations(userId)).thenReturn(new ArrayList<>());

    // mocking addLocation()
    when(gpsUtilMock.getUserLocation(userId)).thenReturn(visitedLocation1);
    when(locationRepositoryMock.addVisitedLocation(Mockito.any())).thenReturn(visitedLocation1);

    Collection<VisitedLocation> actual = locationServices.getAllVisitedLocation(userId);

    // THEN
    verify(locationRepositoryMock, times(1)).getAllVisitedLocations(userId);
    verify(locationRepositoryMock, times(1)).addVisitedLocation(visitedLocation1);
    assertTrue(actual.containsAll(expected));
  }

  @Test
  void getAllUserLatestVisitedLocation() {

    // GIVEN

    // create repository data
    Multimap<UUID, VisitedLocation> data = ArrayListMultimap.create();

    Date date = new Date(3, 3, 3);

    for (int i = 0; i < 2; i++) {
      UUID userId = UUID.randomUUID();
      for (int j = 0; j < 3; j++) {
        VisitedLocation visitedLocation =
            new VisitedLocation(userId, new Location(j, j), new Date(j + 1, j + 1, j + 1));
        data.put(userId, visitedLocation);
      }
    }
    // WHEN
    when(locationRepositoryMock.getAllLocations()).thenReturn(data);

    Collection<VisitedLocation> actual = locationServices.getAllUserLatestVisitedLocation();

    // THEN
    verify(locationRepositoryMock, times(1)).getAllLocations();
    assertThat(actual.size()).isEqualTo(2);

    assertThat(actual.iterator().next().timeVisited).isEqualTo(date);
  }

  @Test
  void getAllUserLatestVisitedLocation_WhenNoData_ThrowResourceNotFoundException() {

    // GIVEN
    Multimap<UUID, VisitedLocation> emptyMultiMap = ArrayListMultimap.create();
    // WHEN
    when(locationRepositoryMock.getAllLocations()).thenReturn(emptyMultiMap);
    // THEN
    assertThrows(
        ResourceNotFoundException.class, () -> locationServices.getAllUserLatestVisitedLocation());
  }

  @Test
  void getNearbyAttractions() {

    // GIVEN

    Collection<Attraction> expected = new LinkedList<>();
    expected.add(new Attraction("close1", "Anaheim", "CA", 0, 0));
    expected.add(new Attraction("close2", "Jackson Hole", "WY", 1, -1));
    expected.add(new Attraction("close3", "Joshua Tree National Park", "CA", -1, -5));
    expected.add(new Attraction("close4", "Kelso", "CA", 5, -5));
    expected.add(new Attraction("close5", "St Joe", "AR", 35.985512, -50));

    // WHEN
    when(locationRepositoryMock.getAllAttractions()).thenReturn(attractions);
    Collection<AttractionWithDistanceDto> actual = locationServices.getNearbyAttractions(0.0, 0.0);
    // THEN
    verify(locationRepositoryMock, times(1)).getAllAttractions();
    assertThat(actual.size()).isEqualTo(expected.size());
    // check if list as the closest attraction in order
    Iterator<AttractionWithDistanceDto> iteratorActual = actual.iterator();
    Iterator<Attraction> iteratorExpected = expected.iterator();
    for (int i = 0; i < 5; i++) {
      assertThat(iteratorActual.next().getAttractionName())
          .isSameAs(iteratorExpected.next().attractionName);
    }
  }

  @Test
  void getNearbyAttractions_WhenNoData_ThrowResourceNotFoundException() {

    // GIVEN
    List<Attraction> emptyList = new ArrayList<>();
    // WHEN
    when(locationRepositoryMock.getAllAttractions()).thenReturn(emptyList);
    // THEN
    assertThrows(
        ResourceNotFoundException.class, () -> locationServices.getNearbyAttractions(1.0, 1.1));
  }

  @Test
  void getAllAttractions() {

    // GIVEN

    // WHEN
    when(locationRepositoryMock.getAllAttractions()).thenReturn(attractions);
    Collection<Attraction> actual = locationServices.getAllAttractions();
    // THEN
    verify(locationRepositoryMock, times(1)).getAllAttractions();
    assertThat(actual.size()).isEqualTo(attractions.size());
  }

  @Test
  void getAllAttractions_WhenNoData_ThrowResourceNotFoundException() {

    // GIVEN
    List<Attraction> emptyList = new ArrayList<>();

    // WHEN
    when(locationRepositoryMock.getAllAttractions()).thenReturn(emptyList);
    // THEN
    assertThrows(ResourceNotFoundException.class, () -> locationServices.getAllAttractions());
  }

  @Test
  void getDistance_ShouldReturnCloseTo98Miles() {

    // pour loc1(0.0,0.0) loc2(1.0,1.0) -> distance ~ 97.75 miles
    Location location1 = new Location(0.0, 0.0);
    Location location2 = new Location(1.0, 1.0);
    Double actual = locationServices.getDistance(location1, location2);
    assertThat(actual).isCloseTo(97.75, withinPercentage(0.5));
  }

  @Test
  void getDistance_ShouldReturnCloseTo9876Miles() {

    //  pour loc1(-50.0,-150.0) loc2(15.0,15.0) -> distance = 9876 miles
    Location location1 = new Location(-50.0, -150.0);
    Location location2 = new Location(15.0, 15.0);
    Double actual = locationServices.getDistance(location1, location2);
    assertThat(actual).isCloseTo(9876.0, withinPercentage(0.5));
  }

  @Test
  void getClosestAttraction() {
    // GIVEN

    // WHEN
    when(locationRepositoryMock.getAllAttractions()).thenReturn(attractions);
    AttractionWithDistanceDto actual = locationServices.getClosestAttraction(2.0, -2.0);
    // THEN
    verify(locationRepositoryMock, times(1)).getAllAttractions();

    assertThat(actual.getDistance())
        .isEqualTo(locationServices.getDistance(new Location(2, -2), new Location(1, -1)));
    assertThat(actual.getAttractionName()).isEqualTo("close2");
  }

  @Test
  void getClosestAttraction_ShouldReturnFar2() {
    // GIVEN

    // WHEN
    when(locationRepositoryMock.getAllAttractions()).thenReturn(attractions);
    AttractionWithDistanceDto actual = locationServices.getClosestAttraction(50, -150);
    // THEN
    verify(locationRepositoryMock, times(1)).getAllAttractions();

    assertThat(actual.getDistance())
        .isEqualTo(
            locationServices.getDistance(
                new Location(50, -150), new Location(31.837551, -110.347382)));
    assertThat(actual.getAttractionName()).isEqualTo("far2");
  }
}
