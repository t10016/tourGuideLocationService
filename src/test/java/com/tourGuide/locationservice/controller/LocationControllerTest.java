package com.tourGuide.locationservice.controller;

import com.tourGuide.locationservice.exception.IllegalArgumentException;
import com.tourGuide.locationservice.exception.ResourceNotFoundException;
import com.tourGuide.locationservice.model.AttractionWithDistanceDto;
import com.tourGuide.locationservice.service.LocationServicesImpl;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;
import java.util.concurrent.CompletableFuture;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = LocationController.class)
class LocationControllerTest {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    private LocationServicesImpl locationServicesMock;


    @Test
    void addLocationValid() throws Exception {

        // GIVEN
        UUID userId = UUID.randomUUID();
        CompletableFuture<VisitedLocation>visitCF=CompletableFuture.completedFuture(new VisitedLocation(userId, new Location(1d, 2d), new Date()));
        // WHEN
        when(locationServicesMock.addLocation(userId))
                .thenReturn(visitCF);
        // THEN
        mockMvc
                .perform(post("/location/add")
                        .param("userId", userId.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void addLocationInvalid() throws Exception {

        // GIVEN
        String invalidUserId = "";
        // WHEN

        // THEN
        mockMvc
                .perform(post("/location/add")
                        .param("userId", invalidUserId)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }


    @Test
    void getAllVisitedLocationValid_ShouldReturn3VisitedLocation() throws Exception {
        // GIVEN
        UUID userId = UUID.randomUUID();
        Collection<VisitedLocation> visitedLocations = new ArrayList<>();
        visitedLocations.add(new VisitedLocation(userId, new Location(1d, 2d), new Date()));
        visitedLocations.add(new VisitedLocation(userId, new Location(1d, 2d), new Date()));
        visitedLocations.add(new VisitedLocation(userId, new Location(1d, 2d), new Date()));
        // WHEN
        when(locationServicesMock.getAllVisitedLocation(userId))
                .thenReturn(visitedLocations);
        // THEN
        mockMvc
                .perform(get("/location/getAll")
                        .param("userId", userId.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)));
    }


    @Test
    void getAllVisitedLocationInvalid() throws Exception {
        // GIVEN
        String userId = "";
        // WHEN

        // THEN
        mockMvc
                .perform(get("/location/getAll")
                        .param("userId", userId))
                .andExpect(status().isBadRequest());

    }

    @Test
    void getAllVisitedLocationValid_WhenNoVisitedLocation_ShouldReturn1VisitedLocation() throws Exception {
        // GIVEN
        UUID userId = UUID.randomUUID();
        Collection<VisitedLocation> visitedLocations = new ArrayList<>();
        visitedLocations.add(new VisitedLocation(UUID.randomUUID(), new Location(1.0, 2.0), new Date()));

        // WHEN
        when(locationServicesMock.getAllVisitedLocation(userId))
                .thenReturn(visitedLocations);
        // THEN
        mockMvc
                .perform(get("/location/getAll")
                        .param("userId", userId.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)));

    }


    @Test
    void getAllLastLocationValidShouldReturn10VisitedLocation() throws Exception {
        // GIVEN

        Collection<VisitedLocation> visitedLocations = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            visitedLocations.add(new VisitedLocation(UUID.randomUUID(), new Location(1d, 2d), new Date()));
        }

        // WHEN
        when(locationServicesMock.getAllUserLatestVisitedLocation())
                .thenReturn(visitedLocations);
        // THEN
        mockMvc
                .perform(get("/getStats")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.length()", is(10)));
    }


    @Test
    void getNearbyAttractionsValid_ShouldReturn5Attractions() throws Exception {
        // GIVEN
        String nameTest = "nameTest";
        String cityTest = "cityTest";
        String stateTest = "stateTest";
        Location location = new Location(75.5, -55.1);

        Collection<AttractionWithDistanceDto> attractions = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            attractions.add(new AttractionWithDistanceDto(nameTest + i, cityTest + i, stateTest + i, new com.tourGuide.locationservice.model.Location(i, -i)));
        }
        // WHEN
        when(locationServicesMock.getNearbyAttractions(location.latitude, location.longitude)).thenReturn(attractions);
        // THEN
        mockMvc.perform(get("/attraction/getNearby")
                        .param("latitude", String.valueOf(location.latitude))
                        .param("longitude", String.valueOf(location.longitude))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    void getNearbyAttractionsInvalid() throws Exception {
        // GIVEN

        // WHEN

        // THEN
        mockMvc.perform(get("/attraction/getNearby")
                        .param("latitude", "")
                        .param("longitude", "-58")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    @Test
    void getNearbyAttractionsValid_WhenArgsValuesAreOutOfExpectation_ShouldThrowIllegalArgumentException() throws Exception {
        // GIVEN
        double nonExpectedLatitude = 1000.0;
        // WHEN

        // THEN
        mockMvc.perform(get("/attraction/getNearby")
                        .param("latitude", String.valueOf(nonExpectedLatitude))
                        .param("longitude", "-58")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertEquals("error, argument out of earth", Objects.requireNonNull(result.getResolvedException()).getMessage()))
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof IllegalArgumentException));
    }


    @Test
    void getAllAttractions() throws Exception {
        // GIVEN
        Collection<Attraction> attractions = new ArrayList<>();
        // WHEN
        when(locationServicesMock.getAllAttractions()).thenReturn(attractions);
        // THEN
        mockMvc.perform(get("/attraction/getAll"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void getAllAttractions_WhenResourceDoesntExist_ShouldThrowResourceNotFoundException() throws Exception {

        // GIVEN

        // WHEN

        doThrow(ResourceNotFoundException.class).when(locationServicesMock).getAllAttractions();
        // THEN
        mockMvc.perform(get("/attraction/getAll"))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ResourceNotFoundException));


    }

    @Test
    void getClosestAttractionValid() throws Exception {

        // GIVEN
        double latitude = 20.0;
        double longitude = 1.5;
//        Map<Attraction, Double> attractionWithDistanceMap = new HashMap<>();

        // WHEN
        when(locationServicesMock.getClosestAttraction(anyDouble(), anyDouble())).thenReturn(new AttractionWithDistanceDto("attractionName","cityTest","state",new com.tourGuide.locationservice.model.Location(latitude,longitude)));
        // THEN
        mockMvc.perform(get("/attraction/getClosest")
                        .param("latitude", String.valueOf(latitude))
                        .param("longitude", String.valueOf(longitude)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void getClosestAttractionInvalidEmptyArgument() throws Exception {

        // GIVEN

        Double longitude = 1.5;
        // WHEN

        // THEN
        mockMvc.perform(get("/attraction/getClosest")
                        .param("latitude", "")
                        .param("longitude", String.valueOf(longitude)))
                .andExpect(status().isBadRequest())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof IllegalArgumentException));

    }

    @Test
    void getClosestAttraction_WhenNoAttraction_ShouldThrowResourceNotFoundException() throws Exception {

        // GIVEN
        double latitude = 20.0;
        double longitude = 1.5;
        // WHEN
        doThrow(ResourceNotFoundException.class).when(locationServicesMock).getClosestAttraction(anyDouble(), anyDouble());
        // THEN
        mockMvc.perform(get("/attraction/getClosest")
                        .param("latitude", String.valueOf(latitude))
                        .param("longitude", String.valueOf(longitude)))
                .andExpect(status().isNotFound())
                .andExpect(result -> assertTrue(result.getResolvedException() instanceof ResourceNotFoundException));
    }
}