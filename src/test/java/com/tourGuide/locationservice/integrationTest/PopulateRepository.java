package com.tourGuide.locationservice.integrationTest;

import com.tourGuide.locationservice.repository.LocationRepository;
import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;

public class PopulateRepository {

    private final LocationRepository locationRepository;

    public PopulateRepository(LocationRepository locationRepository, int internalUserNumber) {
        this.locationRepository = locationRepository;
        generateUserLocationHistory(internalUserNumber);
    }

    /**********************************************************************************
     * Methods Below will generate data for Internal Testing.
     **********************************************************************************/

    private void generateUserLocationHistory(int internalUserNumber) {
        int count = 0;
        for (int i = 0; i < internalUserNumber; i++) {
            String userNumber = String.format("%06d", i);
            UUID userId = UUID.fromString("0000-00-00-00-" + userNumber);
            IntStream.range(0, 1)
                    .forEach(
                            j ->
                                    locationRepository.addVisitedLocation(
                                            new VisitedLocation(
                                                    userId,
                                                    new Location(generateRandomLatitude(), generateRandomLongitude()),
                                                    getRandomTime())));
            count++;
        }
        System.out.println(count + " user location history.");
    }

    @SuppressFBWarnings("DMI_RANDOM_USED_ONLY_ONCE")
    private double generateRandomLongitude() {
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    @SuppressFBWarnings("DMI_RANDOM_USED_ONLY_ONCE")
    private double generateRandomLatitude() {
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    @SuppressFBWarnings("DMI_RANDOM_USED_ONLY_ONCE")
    private Date getRandomTime() {
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

}
