package com.tourGuide.locationservice.integrationTest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tourGuide.locationservice.model.AttractionWithDistanceDto;
import com.tourGuide.locationservice.repository.LocationRepository;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class LocationServiceIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private LocationRepository locationRepository;

    @BeforeAll
    static void beforeAll() {
        Locale.setDefault(Locale.US);
    }

    @Test
    void addLocation() throws Exception {

        UUID userId = UUID.randomUUID();

        mockMvc
                .perform(post("/location/add")
                        .param("userId", userId.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));

        assertTrue(locationRepository.getAllLocations().containsKey(userId));
        assertEquals(1, locationRepository.getAllLocations().get(userId).size());

        mockMvc
                .perform(post("/location/add")
                        .param("userId", "")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getAllVisitedLocation() throws Exception {

        UUID userId = UUID.randomUUID();
        for (int i = 1; i < 5; i++) {
            locationRepository.addVisitedLocation(new VisitedLocation(userId, new Location(i, i), new Date(i, i, i)));
        }

        mockMvc
                .perform(get("/location/getAll")
                        .param("userId", userId.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(4)));

        UUID newUser = UUID.randomUUID();

        assertTrue(locationRepository.getAllLocations().get(newUser).isEmpty());

        mockMvc
                .perform(get("/location/getAll")
                        .param("userId", newUser.toString())
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)));
    }

    @Test
    void getAllLastLocation() throws Exception {
        UUID userId = UUID.randomUUID();
        // create 100 visitedLocation (1 different userId per location)
        new PopulateRepository(locationRepository,100);
        // add a new entry with 5 visited locations
        for (int i = 1; i < 5; i++) {
            locationRepository.addVisitedLocation(new VisitedLocation(userId, new Location(i, i), new Date(i, i, i)));
        }
        MvcResult mvcResult = mockMvc
                .perform(get("/getStats")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                // check if we only get 1 visitedLocation per user
                .andExpect(jsonPath("$.length()", is(101)))
                .andReturn();

        // check if the most recent visited location for our user is present in json

        String userLastLocation = "\"" + userId + "\"" + ":{\"longitude\":4.0,\"latitude\":4.0}";
        assertTrue(mvcResult.getResponse().getContentAsString().contains(userLastLocation));

    }

    @Test
    void getNearbyAttractions() throws Exception {

        // GIVEN
        Location locationTest = new Location(0.0, 0.0);

        List<String> expected = new LinkedList<>();
        expected.add("Franklin Park Zoo");
        expected.add("Bronx Zoo");
        expected.add("Flatiron Building");
        expected.add("Union Station");
        expected.add("Roger Dean Stadium");

        MvcResult result = mockMvc.perform(get("/attraction/getNearby")
                        .param("latitude", String.valueOf(locationTest.latitude))
                        .param("longitude", String.valueOf(locationTest.longitude)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        // check if result is correct and ordered
        ObjectMapper objectMapper = new ObjectMapper();
        AttractionWithDistanceDto[] attractions = objectMapper.readValue(result.getResponse().getContentAsString(), AttractionWithDistanceDto[].class);

        Iterator<String> expectedIterator = expected.iterator();

        Arrays.stream(attractions).forEach(i -> assertEquals(i.getAttractionName(), expectedIterator.next()));
    }

    @Test
    void getClosestAttraction() throws Exception {
        // GIVEN
        Location locationTest = new Location(28, -81);

//        Attraction("Cinderella Castle", "Orlando", "FL", 28.419411, -81.5812));

        MvcResult result = mockMvc.perform(get("/attraction/getClosest")
                        .param("latitude", String.valueOf(locationTest.latitude))
                        .param("longitude", String.valueOf(locationTest.longitude)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper obm = new ObjectMapper();
        AttractionWithDistanceDto response = obm.readValue(result.getResponse().getContentAsString(), AttractionWithDistanceDto.class);
        assertEquals("Cinderella Castle", response.getAttractionName());
        System.out.println(response);
    }


}
