### Start application

* copy/clone this project on your computer.
* cd PATH/TO/PROJECT/ROOT where build.gradle is present
* run: ./gradlew bootJar
* run: docker build . -t tourguide/locationservice
* Access endpoints on localhost:8081/

## Technical:

1. Java : 17
2. Gradle 7+
3. SpringBoot : 2.6.6
4. Docker

## EndPoints

* POST: "/location/add" 
  * parameter: userId 
  * return: actual VisitedLocation
* GET: "/location/getAll" 
  * parameter: userId
  * return: user's Collection<VisitedLocation> 
* GET: "/getStats" 
  * parameter: no_parameter
  * return: Map<UUID, Location> the last Location for each user
* GET: "/attraction/getNearby" 
  * parameters: latitude, longitude
  * return: Collection<AttractionWithDistanceDto>
* GET: "/attraction/getAll" 
  * parameter: no_parameter
  * return: Collection<Attraction>
* GET: "/attraction/getClosest" 
  * parameters: latitude, longitude
  * return: AttractionWithDistanceDto

